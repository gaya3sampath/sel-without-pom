package learnannotationweek6;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class RoutineWork {
  @Test
  public void dowork() {
	  System.out.println("dowork");
  }
  @Test
  public void takebraek() {
	  System.out.println("takebreak");
  }
  
  @BeforeMethod
  public void GetintoOdc() {
	  System.out.println("GetintoOdc");
  }

  @AfterMethod
  public void GetOuttoODC() {
	  
	  System.out.println("GetOuttoODC");
  }

  @BeforeClass
  public void wearformals() {
	  System.out.println("wearformals");
  }

  @AfterClass
  public void wearcasuals() {
	  
	  System.out.println("wearcasuals");
  }

  @BeforeTest
  public void brush() {
	  System.out.println("brushing");
  }

  @AfterTest
  public void beforesleepbrush() {
	  System.out.println("beforesleepbrush");
  }

  @BeforeSuite
  public void wakeup() {
	  System.out.println("wakeup");
  }

  @AfterSuite
  public void sleep() {
	  System.out.println("sleep");
  }

}
