package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepImplementation {
      ChromeDriver driver;
      @Given("Launch the Browser")
      public void launchTheBrowser() {
          System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
          driver = new ChromeDriver();
          
      }

      @And("Load the Url")
      public void loadTheUrl() {
    	  driver.get("http://leaftaps.com/opentaps/control/main");
    	  
          
      }

      @And("Maximize the window")
      public void maximizeTheWindow() {
    	  driver.manage().window().maximize();
         
      }

      @And("Set Wait")
      public void setWait() {
    	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
          
      }

      @And("Enter Username as (.*)")
      public void enterUsername(String data) {
    	  driver.findElementById("username").sendKeys(data);
          
      }

      @And("Enter Password as (.*)")
      public void enterPassword(String data) {
    	  driver.findElementById("password").sendKeys(data);
    	  
}

      @When("click Login")
      public void clickLogin() {
    	  driver.findElementByClassName("decorativeSubmit").click();
    	  
          
      }

      @Then("Verify success")
      public void verifySuccess() {
    	  System.out.println("Verify sucess");
          
      }

      @Then("Logout")
      public void logout() {
    	  driver.findElementByClassName("decorativeSubmit").click();
      }
      
      @Then("Close Browser")
      public void closeBrowser() {
          driver.close();
      }
          
      }


