package tcw6hw3day1;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TS4_DeLd extends ProjectMethods  {
	
	//Find lead from TestLeaf and Delete the lead
	
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TS4_DeLd";
		testDesc ="This test case for Delete the Lead";
		author ="GayathriD";
		category = "Sanity";
	}
	@Test(dataProvider = "fetchData4", dependsOnMethods = "tcw6hw3day1.TS3_DpLd.duplicateLead")
	
	public void deleteLead(String fname4, String lName4, String cName4) throws InterruptedException {
	clickWithOutSnap(locateElement("link", "CRM/SFA"));	
	clickWithOutSnap(locateElement("link", "Leads"));
	clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
	type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fname4);
	type(locateElement("xpath", "(//input[@name='lastName'])[3]"), lName4);
	type(locateElement("xpath", "(//input[@name='companyName'])[2]"), cName4);
	clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[6]"));
	Thread.sleep(3000);
	clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
	clickWithOutSnap(locateElement("xpath", "//a[text()='Delete']"));
				
	
	}
		
	@DataProvider(name = "fetchData4")
	public  String[][] getData() {
		String[][] data4 = new String[2][3];
		data4[0][0] = "Gaya3";
		data4[0][1] = "S";
		data4[0][2] = "Htss";
												
		data4[1][0] = "Pree";
		data4[1][1] = "T";
		data4[1][2] = "HiTech";
						
		return data4;
	
}


}
