package tcw6hw3day1;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TS1_CrLd extends ProjectMethods {
	
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TS1_CrLd";
		testDesc ="Create a new lead in leaftaps";
		author ="GayathriC";
		category = "Smoke";
	}
	@Test(dataProvider = "fetchData")
		
	public void createLead(String cName, String fName, String lName) {
		click(locateElement("link", "CRM/SFA"));
		clickWithOutSnap(locateElement("xpath", "//a[text() = 'Create Lead']"));
		click(locateElement("link", "Create Lead"));
		type(locateElement("id", "createLeadForm_companyName"), cName);
		type(locateElement("id", "createLeadForm_firstName"), fName);
		type(locateElement("id", "createLeadForm_lastName"), lName);
		type(locateElement("id", "createLeadForm_primaryEmail"), "gaya3@yahoo.com");
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), "9444894901");
		click(locateElement("name", "submitButton"));
	}
	
	@DataProvider(name = "fetchData")
	public  String[][] getData() {
		String[][] data = new String[2][3];
		data[0][0] = "Htss";
		data[0][1] = "Gaya3";
		data[0][2] = "S";
		
		data[1][0] = "HiTech";
		data[1][1] = "Pree";
		data[1][2] = "T";
		
		return data;

}
	
}
