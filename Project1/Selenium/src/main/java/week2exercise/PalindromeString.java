package week2exercise;

import java.util.Scanner;

public class PalindromeString {

	public static void main(String[] args) {
		// TO find given string is palindrome or not
	
	   String str, rev= "";
	   Scanner sc = new Scanner(System.in);
	   System.out.println("Enter the string");
	   str = sc.next();
	   int length = str.length();
	   
	   for(int i = length -1; i>=0; i--) 
		   rev = rev + str.charAt(i);
	   
	   if(str.equals(rev))
		   System.out.println("Given String is palindrome");
	   
	   else
		   System.out.println("Given string is not palindrome");
			   
		   
		   
	   }
	   
	}