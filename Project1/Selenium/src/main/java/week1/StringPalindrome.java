package week1;

public class StringPalindrome {

	public static void main(String[] args) {
		// To check whether given string is palindrome or not
		
		String org = new String("madame");
		String des = org;
		String rev="";
		
		for(int i=des.length()-1; i>=0;i--)   //this is important line
			rev = rev+des.charAt(i);
		
       if(rev.equals(des))
    	   System.out.println(rev+ " String is palindrome");
       else
    	   System.out.println(rev+ " String is not palindrome");
	}

}
