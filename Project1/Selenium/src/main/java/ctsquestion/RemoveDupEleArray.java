package ctsquestion;

import java.util.HashSet;
import java.util.Set;

public class RemoveDupEleArray {

	public static void main(String[] args) {
		// TO Remove duplicate element from an array
		
		String[] input = { "ab", "cd", "ab", "de", "cd" };
		
		Set<String> duplicates = new HashSet<String>(); 
		
		for (int i = 0; i < input.length; i++) { 
			for (int j = 1; j < input.length; j++) {
				if (input[i] == input[j] && i != j) {
				 // duplicate element found 
					duplicates.add(input[i]); 
					break;
				}
			}
			
			
		}

		System.out.println(duplicates);

	}

}
