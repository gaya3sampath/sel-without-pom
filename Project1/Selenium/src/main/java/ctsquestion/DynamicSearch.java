package ctsquestion;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DynamicSearch {

	public static void main(String[] args) {
		// Dynamic Search on Google
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.google.co.in/");
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementByName("q").sendKeys("jira");
		List<WebElement> li =driver.findElementsByXPath("//ul[@role='listbox']//li/descendant::div[contains(text(),'jira')]");
		int count = li.size();
		
		for(int i=0; i<count; i++) {
			System.out.println(li.get(i).getText());
			if(li.get(i).getText().contains("jira tutorial")) {
				li.get(i).click();
			}
		}
			

	}

}
