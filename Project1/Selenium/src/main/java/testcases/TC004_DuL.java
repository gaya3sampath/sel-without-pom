package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethodsFirst;

public class TC004_DuL extends ProjectMethodsFirst  {
	//Find lead duplicate the lead in from Leads tab
	
		@BeforeClass(groups = "config")
		public void setData() {
			testcaseName ="TC004_DuL";
			testDesc ="Create a new lead in leaftaps";
			author ="Gayatri";
			category = "Smoke";
		}
		@Test
		
		public void createLead() throws InterruptedException {
		clickWithOutSnap(locateElement("link", "CRM/SFA"));	
		clickWithOutSnap(locateElement("link", "Leads"));
		clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Gaya3");
		type(locateElement("xpath", "(//input[@name='lastName'])[3]"), "S");
		type(locateElement("xpath", "(//input[@name='companyName'])[2]"), "Htss");
		clickWithOutSnap(locateElement("xpath", "(//button[@class='x-btn-text'])[6]"));
		Thread.sleep(3000);
		clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		clickWithOutSnap(locateElement("xpath", "(//a[@class='subMenuButton'])[1]"));
		Thread.sleep(3000);
		selectDropDownUsingText(locateElement("xpath", "//select[@name='ownershipEnumId']"), "Public Corporation", "visible");
		clickWithOutSnap(locateElement("xpath", "//input[@class='smallSubmit']"));
		
		}
			
		
		
	}
