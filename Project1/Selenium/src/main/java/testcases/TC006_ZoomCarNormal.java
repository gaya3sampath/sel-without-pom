package testcases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TC006_ZoomCarNormal {


public static void main(String[] args) throws InterruptedException {
	
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =  new ChromeDriver();
		
		// Maximize Window
		driver.manage().window().maximize();
		// Launch URL
		
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("//div[contains(text(),'Thuraipakkam')]").click();
		driver.findElementByXPath("//button[contains(text(),'Next')]").click();
		String textStartD = driver.findElementByXPath("(//div[@class='days']/div)[2]").getText();
		driver.findElementByXPath("(//div[@class='days']/div)[2]").click();
		driver.findElementByXPath("//button[contains(text(),'Next')]").click();
		Thread.sleep(3000);
		String textEndD = driver.findElementByXPath("(//div[@class='days']/div)[1]").getText();
		if(textStartD.equalsIgnoreCase(textEndD)) {
			System.out.println("Text is verified"+textStartD);
		}else {
			System.out.println("Text is verified"+textStartD);
		}
		driver.findElementByXPath("//button[contains(text(),'Done')]").click();
		Thread.sleep(3000);
		List<WebElement> totRes  = driver.findElements(By.xpath("//div[@class='price']"));
		System.out.println(totRes.size());
		List<Integer> priceList = new ArrayList<Integer>();
		for(WebElement e: totRes) {
			//System.out.println(e.getText());
			String text = e.getText().replaceAll("\\D", "");
			System.out.println(text);
			priceList.add(Integer.parseInt(text));
			
		}
		System.out.println("Final List Size"+priceList.size());
		
		Collections.sort(priceList);
		Integer max = Collections.max(priceList);
		System.out.println("Max value"+max);
		String text = driver.findElementByXPath("(//div[contains(text(),'"+max+"')]/../../preceding-sibling::div)[2]/h3").getText();
		System.out.println("Result Car is:"+text);
		driver.findElementByXPath("(//div[contains(text(),'"+max+"')])[1]/../button").click();
		driver.quit();
		
}

}
