package testcases;

import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC003_ML extends ProjectMethods {
	
	 //Merge two leads in TestLeaf 
	
		@BeforeClass(groups = "config")
		public void setData() {
			testcaseName ="TC003_ML";
			testDesc ="Merge the leads in leaftaps";
			author ="Gayatri";
			category = "Smoke";
		}
	    
		@Test
		public void createlead() throws InterruptedException {
			
			clickWithOutSnap(locateElement("link", "CRM/SFA"));	
			clickWithOutSnap(locateElement("link", "Leads"));
			clickWithOutSnap(locateElement("xpath", "//a[text()='Merge Leads']"));
			clickWithOutSnap(locateElement("xpath", "//input[@id='ComboBox_partyIdFrom']/following::img[1]"));
			switchToWindow(1);
			type(locateElement("xpath", "//input[@name='firstName']"), "Gaya3");
			type(locateElement("xpath", "//input[@name='lastName']"), "S");
			type(locateElement("xpath", "//input[@name='companyName']"), "Htss");
			clickWithOutSnap(locateElement("xpath", "(//button[@class='x-btn-text'])[1]"));
			Thread.sleep(2000);
			clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
			switchToWindow(0);
			clickWithOutSnap(locateElement("xpath", "//input[@id='ComboBox_partyIdTo']/following::img[1]"));
			switchToWindow(1);
			type(locateElement("xpath", "//input[@name='firstName']"), "Pree");
			type(locateElement("xpath", "//input[@name='lastName']"), "S");
			type(locateElement("xpath", "//input[@name='companyName']"), "HiTech");
			clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[1]"));
			Thread.sleep(3000);
			clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
			switchToWindow(0);
			WebElement merbutton = locateElement("xpath", "//a[@class='buttonDangerous']");
			try {
				click(merbutton);
			}catch (UnhandledAlertException f) {
				acceptAlert();
			}
						
		}

}
