package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethodsFirst;

public class TC001_CL extends ProjectMethodsFirst {
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TC001_CL";
		testDesc ="Create a new lead in leaftaps";
		author ="Gayatri";
		category = "Smoke";
	}
	@Test
		
	public void createLead() {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Create Lead"));
		type(locateElement("id", "createLeadForm_companyName"), "Htss");
		type(locateElement("id", "createLeadForm_firstName"), "Gaya3");
		type(locateElement("id", "createLeadForm_lastName"), "S");
		type(locateElement("id", "createLeadForm_primaryEmail"), "gaya3@yahoo.com");
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), "9444894901");
		click(locateElement("name", "submitButton"));
	}
}
