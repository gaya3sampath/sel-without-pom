package week6exercise;

import java.util.Scanner;

public class FindPerfectNumber {

	public static void main(String[] args) {
		/*A perfect number is a positive integer that is equal to the sum of its proper positive divisors, 
		that is, the sum of its positive divisors excluding the number itself (also known as its aliquot sum). 
		Equivalently, a perfect number is a number that is half the sum of all of its positive divisors (including itself)
		eg:28 is perfect number - 1+2+4+7+14=28
		   and (1+2+4+7+14+28)/2=28*/
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number");
		int n = sc.nextInt();
		int sum =0;
		
		//Here we can also use i<=n/2
		for(int i=1; i<n; i++) {
			if(n%i ==0) {
				sum=sum+i;
			}
		}
		if(sum==n) {
			System.out.println("Given number is perfect");
		}else {
		
System.out.println("Given number is not perfect");
	}
    
}
int divisor(int x)
{
	return x;
}
}
