package week6exercise;

import java.util.regex.Pattern;

public class VerifyStringDigit {

	public static void main(String[] args) {
		// To verify the string has Exactly 5 digit
		String [] inputs = {"1234.5", "123.67", "as156", "98765", "78654.00"};
		Pattern digitpattern = Pattern.compile("\\d{5}");
		
		for(String input: inputs) {
			System.out.println("string " +input+ " has exactly 5 digit:" +digitpattern.matcher(input).matches());
			
		}
		

	}

}
