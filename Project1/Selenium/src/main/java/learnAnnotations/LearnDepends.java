package learnAnnotations;

import org.testng.annotations.Test;

public class LearnDepends {
	// This test case used to learn about Depends On Methods

	@Test(dependsOnMethods = "learnAnnotations.Pri.met2", alwaysRun = true)
	public void met1() {
		System.out.println("met1");
	}
	@Test()
	public void met2() {
		System.out.println("met2");
		throw new RuntimeException(); //Customzed created exception
	}
	@Test()
	public void met4() {
		System.out.println("met4");
	}
}
