package learnAnnotations;

import org.testng.annotations.Test;

public class LearnPriority {
	// To learn about Priority annotation. Based on Priority number it will run
	
	@Test(priority=7)
	public void met1(){
		System.out.println("met1");
	}

	@Test(priority=-2)
	public void met2(){
		System.out.println("met2");
	}
	
	@Test(priority=5)
	public void met3(){
		System.out.println("met3");
	}
	
	@Test(priority=1)
	public void met4(){
		System.out.println("met4");
	}
	
}
