package hwweek6;

import java.util.Scanner;

public class SwapString {

	public static void main(String[] args) {
		// TO swap two strings without using third variable
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the 1st string");
		String inp1 = sc.nextLine();
		System.out.println("Enter the 2nd string");
		String inp2 = sc.nextLine();
		
		inp1 = inp1 + inp2;   // sum both the string		
		inp2 = inp1.substring(0, inp1.length() - inp2.length()); // inp2 swapped to inp1
		inp1 = inp1.substring(inp2.length());
		System.out.println("After swapped the 1st output is: " +inp1+ " and the 2nd output is: "+inp2);
		
	}

}
