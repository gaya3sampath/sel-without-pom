package tesatcasesweek6;

import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods   {
	
		 //Merge two leads in TestLeaf 
		
			@BeforeClass(groups = "config")
			public void setData() {
				testcaseName ="TC003_MergeLead";
				testDesc ="Merge the leads in leaftaps";
				author ="Gayatri2";
				category = "Smoke";
			}
		    
			@Test(dataProvider = "fetchData1")
			public void createlead(String fName1, String lName1, String cName1) throws InterruptedException {
				
				clickWithOutSnap(locateElement("link", "CRM/SFA"));	
				clickWithOutSnap(locateElement("link", "Leads"));
				clickWithOutSnap(locateElement("xpath", "//a[text()='Merge Leads']"));
				clickWithOutSnap(locateElement("xpath", "//input[@id='ComboBox_partyIdFrom']/following::img[1]"));
				switchToWindow(1);
				type(locateElement("xpath", "//input[@name='firstName']"), fName1);
				type(locateElement("xpath", "//input[@name='lastName']"), lName1);
				type(locateElement("xpath", "//input[@name='companyName']"), cName1);
				clickWithOutSnap(locateElement("xpath", "(//button[@class='x-btn-text'])[1]"));
				Thread.sleep(2000);
				clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
				switchToWindow(0);
				clickWithOutSnap(locateElement("xpath", "//input[@id='ComboBox_partyIdTo']/following::img[1]"));
				switchToWindow(1);
				type(locateElement("xpath", "//input[@name='firstName']"), fName1);
				type(locateElement("xpath", "//input[@name='lastName']"), lName1);
				type(locateElement("xpath", "//input[@name='companyName']"), cName1);
				clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[1]"));
				Thread.sleep(3000);
				clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
				switchToWindow(0);
				WebElement merbutton = locateElement("xpath", "//a[@class='buttonDangerous']");
				try {
					click(merbutton);
				}catch (UnhandledAlertException f) {
					acceptAlert();
				}
				
								
			}
			@DataProvider(name = "fetchData1")
			public  String[][] getData() {
				String[][] data1 = new String[2][3];
				data1[0][0] = "Gaya3";
				data1[0][1] = "S";
				data1[0][2] = "Htss";
				
				data1[1][0] = "Pree";
				data1[1][1] = "T";
				data1[1][2] = "HiTech";
				
				return data1;

	}


}
