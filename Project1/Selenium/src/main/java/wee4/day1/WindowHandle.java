package wee4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) throws IOException, InterruptedException {
		// In IRCTC page and select contact us and print that window title
		
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				ChromeDriver driver = new ChromeDriver();
				//Load the URL
				driver.get("http://irctc.co.in");
				//Maximise window screen
				driver.manage().window().maximize();
				driver.findElementByLinkText("AGENT LOGIN").click();
				driver.findElementByLinkText("Contact Us").click();
				Set<String> windowlist = driver.getWindowHandles();
				List<String> listofWindow = new ArrayList<>();
				listofWindow.addAll(windowlist);
				driver.switchTo().window(listofWindow.get(1));
				//Maximise the 2nd window'
				driver.manage().window().maximize();
				System.out.println(driver.getTitle());
				
				File screen = driver.getScreenshotAs(OutputType.FILE);
				File desf = new File("./snaps/img.png");
				FileUtils.copyFile(screen, desf);
				Thread.sleep(10000);
				driver.quit();
				
		}

}
