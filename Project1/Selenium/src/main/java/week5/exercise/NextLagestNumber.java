package week5.exercise;

import java.util.Scanner;

public class NextLagestNumber {

	public static void main(String[] args) {
		// TO find largest number that is less than the given number also should not contain given digit
		//for eg: if given num is 154 and given digit is 5, then output is less than 157 and also not contain 5, then ans is 149
		
		Scanner sc = new Scanner(System.in);
		System.out.println("*** One Nmber One Digit ***");
		System.out.println("Enter the number");
		int n = sc.nextInt();
		System.out.println("enter the digit");
		int digit = sc.nextInt();
		char digitchar = Integer.toString(digit).charAt(0);

        for (int i = n-1; i>0; i--) {
        	if(Integer.toString(i).indexOf(digitchar)==-1) {
        		System.out.println("Answer is " +i);
        		break;
        		
        	}
        	        	
        }
		sc.close();
		
	}

}
