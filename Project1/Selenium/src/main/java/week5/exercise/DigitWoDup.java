package week5.exercise;

import java.util.Scanner;

public class DigitWoDup {

	public static void main(String[] args) {
		// TO display the digits of the number in ascending order without duplicate
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no.of element you want in array");
		int n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter all the elements");
		
		for(int i=0; i<n; i++) {
			for(int j=0; j<n; j++) {
				if(arr[i] == arr[j]) {
					
					System.out.println("Digit without duplicate is: " +arr[i]);
					break;				
				}
					
			}
			
		}	
		
	}

}
