package testque;

import java.util.Arrays;

public class AcendDigit {

	public static void main(String[] args) {
		/*Write a simple java program to rearrange a given number
		in ascending order of their digits*/
		 
		/*int inp = 19961763;
		//Convert integer to string
		int len=Integer.toString(inp).length();
        int[] arr=new int[len];
        for(int i=0;i<len;i++)
        {
            arr[i]=inp%10;
            inp=inp/10;
        }
        //sorting the array
        Arrays.sort(arr);
        int num=0;
        for(int i=0;i<len;i++)
        {
            num=(num*10)+arr[i];
        }
        System.out.println(num);
    }    
}*/

		int number = 192637;
        int sortedNumber = 0;
        for (int i = 0; i<=9; i++)
        {
            int tmpNumber = number;
            while (tmpNumber > 0)
            {
                int digit = tmpNumber % 10;             
                if (digit == i)
                {
                    sortedNumber *= 10;
                    sortedNumber += digit;
                }
                tmpNumber /= 10;                
            }               
        }
        System.out.println(sortedNumber); // prints 54322.
}
}
      
