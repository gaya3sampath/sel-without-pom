package testque;

import java.util.HashSet;
import java.util.Set;

public class DuplicateString {

	public static void main(String[] args) {
		// TO find duplicate string an array
		
		String[] names = { "Java", "JavaScript", "Python", "C", "Ruby", "Java" };
		
		System.out.println("Duplicate elements from array using HashSet data structure"); 
		Set<String> store = new HashSet<>(); 
		for (String name : names) { 
			if (store.add(name)) { 
			System.out.println("found a duplicate element in array : " + name); }
		}

	}

}
